package tw.com.twgame.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tw.com.twgame.dao.PaymentDao;
import tw.com.twgame.model.Payment;
import tw.com.twgame.model.PaymentForView;

@Service
@Transactional
public class PaymentManager {
	@Autowired
	private PaymentDao paymentDao;

	public List<PaymentForView> findByViewCondition(String payOrderId, int payStatus, int restoreStatus, String carId, Timestamp startCreateTime, Timestamp endCreateTime, int pageNo, int pageSize) {
		return paymentDao.findByViewCondition(payOrderId, payStatus, restoreStatus, carId, startCreateTime, endCreateTime, pageNo, pageSize);
	}

	public int findByViewConditionCount(String payOrderId, int payStatus, int restoreStatus, String carId, Timestamp startCreateTime, Timestamp endCreateTime) {
		return paymentDao.findByViewConditionCount(payOrderId, payStatus, restoreStatus, carId, startCreateTime, endCreateTime);
	}

	public PaymentForView findOneById(long id) {
		return paymentDao.findOneById(id);
	}

	public int updatePayStatus(long id, int payStatus) {
		return paymentDao.updatePayStatus(id, payStatus);
	}

	public int updatePayStatus(long id, int nowPayStatus, int newPayStatus) {
		return paymentDao.updatePayStatus(id, nowPayStatus, newPayStatus);
	}

	public void updateRemarkById(long id, String remark) {
		paymentDao.updateRemarkById(id, remark);
	}

	public Payment add(final Payment pay) {
		return paymentDao.add(pay);
	}
	
	public Payment addPaid(final Payment pay, String channel) {
		return paymentDao.addPaid(pay, channel);
	}
	
	public int countPay(String uid) {
		return paymentDao.countPay(uid);
	}
	
	public void updateRestore(String[] payList, Date date, int rstatus) {
		paymentDao.updateRestore(payList, date, rstatus);
	}

	public void updateCarIdDerverID(Long id, String carId, Long derverId) {
		paymentDao.updateCarIdDerverID(id, carId, derverId);
	}

	public Payment findByPayFromPayOrderId(String payFrom, String orderId) {
		return paymentDao.findByPayFromPayOrderId(payFrom, orderId);
	}

	public PaymentForView findByOrderId(String orderId) {
		return paymentDao.findByOrderId(orderId);
	}
}
