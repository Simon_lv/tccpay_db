package tw.com.twgame.service;

import java.util.HashMap;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.com.twgame.dao.AdminDao;
import tw.com.twgame.model.Admin;



/**
 * User manager class
 * @author Donald
 * time:2013-10-30 16:45:58
 * @version 1.3
 */
@Service
@Transactional
public class AdminManager {
	
	@Autowired
   private AdminDao adminDao;

	public AdminDao getUserDao() {
		return adminDao;
	}

	@Transactional(readOnly = true)
	public Admin findByUsername(String username) {
		Admin admin = adminDao.findByUsername(username);
		if (admin != null && admin.getStatus() == Admin.STATUS_INVALID) {
			//已停權
			admin = null;
		}
		return admin;
	}
	
	public void saveUser(HashMap<String, String> map){
		adminDao.saveUser(map);
	}
    
	 public Page<Admin> queryUserList(Page<Admin> page,HashMap<String, String> queryParams){
		 return adminDao.queryUserList(page, queryParams);
	 }
	 
	 public int updateUserStatus(int id,int status){
		 return adminDao.updateUserStatus(id, status);
	 }
	 
	 public Admin queryUserinfo(int id){
		 return adminDao.queryUserinfo(id);
	 }
	 
	 public  void updateUserInfo(HashMap<String, String> map){
		 adminDao.updateUser(map);
	 }
	 
	 public Admin loginSystem(String name,String password){
		 return adminDao.loginSystem(name, password);
	 }
	
	 
	 public int checkSameUser(HashMap<String, String> map){
		 return adminDao.checkSameUser(map);
	 }
	 
	 /**
	  * change user password
	  * */
	 public int updatePwd(long id,String newPwd){
		   return adminDao.updatePwd(id, newPwd);
	 }
	 
	 /**
	  * change user info.
	  * */
	 public int updateAdmin(HashMap<String, String>map){
	    	return adminDao.updateAdmin(map);
	 }

}
