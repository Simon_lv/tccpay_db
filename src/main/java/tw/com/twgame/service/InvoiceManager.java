package tw.com.twgame.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tw.com.twgame.dao.InvoiceDao;
import tw.com.twgame.model.Invoice;

@Service
@Transactional
public class InvoiceManager {
	@Autowired
	private InvoiceDao invoiceDao;
	
	public Invoice add(Invoice invoice) {
		return invoiceDao.add(invoice);
	}

}
