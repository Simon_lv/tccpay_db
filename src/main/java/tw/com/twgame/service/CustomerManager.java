package tw.com.twgame.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tw.com.twgame.dao.CustomerDao;
import tw.com.twgame.model.Customer;

@Service
@Transactional
public class CustomerManager {
	@Autowired
	private CustomerDao customerDao;
	
	public Customer findByPayFrom(String payFrom) {
		return customerDao.findByPayFrom(payFrom);
	}
}
