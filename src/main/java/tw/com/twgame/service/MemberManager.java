package tw.com.twgame.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tw.com.twgame.dao.MemberDao;
import tw.com.twgame.model.Member;

@Service
@Transactional
public class MemberManager {
	@Autowired
	private MemberDao memberDao;
	
	public int authenticate(String uid) {
		return memberDao.authenticate(uid);
	}
	
	public Member add(final Member member) {
		return memberDao.add(member);
	}
	
	public Member findByUid(final String uid) {
		return memberDao.findByUid(uid);
	}

	public Member findByUidAndPayFrom(String uid, String payFrom) {
		return memberDao.findByUidAndPayFrom(uid, payFrom);
	}

	public int update(Member member) {
		return memberDao.update(member);
	}

	public Member findByNameMobilePayFrom(String name, String mobile, String payFrom) {
		return memberDao.findByNameMobilePayFrom(name, mobile, payFrom);
	}

	public void updateUid(String uid, String name, String mobile, String payFrom) {
		memberDao.updateUid(uid, name, mobile, payFrom);
	}

//	public Member addByOrderApi(Member member) {
//		return memberDao.addByOrderApi(member);
//	}
}
