package tw.com.twgame.service;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.com.twgame.dao.DriverDao;
import tw.com.twgame.model.Driver;

@Service
@Transactional
public class DriverManager {

	@Autowired
	private DriverDao driverDao;

	public DriverDao getDriverDao() {
		return driverDao;
	}
	
	public Page<Driver> queryUserList(Page<Driver> page,HashMap<String, String> queryParams){
		StringBuffer strSQL = new StringBuffer("SELECT d.id,d.car_id,d.name,d.password,d.mobile,d.credit,d.non_restore,d.create_time,d.can_sell,d.status, d.is_tester FROM driver d where status=1"); 
		StringBuffer pageSQL = new StringBuffer("SELECT count(d.id) FROM driver d where status=1"); 
		
		if(!StringUtils.isBlank(queryParams.get("carId"))){
			strSQL.append(" and d.car_id='"+queryParams.get("carId")+"'");
			pageSQL.append(" and d.car_id='"+queryParams.get("carId")+"'");
		}
		
		if(StringUtils.isNotBlank(queryParams.get("name"))){
			strSQL.append(" and d.name like'%"+queryParams.get("name")+"%'");
			pageSQL.append(" and d.name like '%"+queryParams.get("name")+"%'");
		}
		if(StringUtils.isNotBlank(queryParams.get("mobile"))){
			strSQL.append(" and d.mobile ='"+queryParams.get("mobile")+"'");
			pageSQL.append(" and d.mobile ='"+queryParams.get("mobile")+"'");
		}
		if(StringUtils.isNotBlank(queryParams.get("canSell"))){
			strSQL.append(" and d.can_sell ='"+queryParams.get("canSell")+"'");
			pageSQL.append(" and d.can_sell ='"+queryParams.get("canSell")+"'");
		}
		
		strSQL.append(" ORDER BY d.id desc limit "+page.getFirst()+","+page.getPageSize()+"");
	
		return driverDao.queryUserList(page,strSQL.toString(),pageSQL.toString());
	 }
	
	public Driver add(Driver d) {
		final String sql = "insert into driver(car_id,name,mobile,password,credit,non_restore,create_time,can_sell,status)values(?,?,?,?,?,?,?,?,?);";
		Object[] parmas={d.getCarId(),d.getName(),d.getMobile(),d.getPassword(),d.getCredit(),d.getNonRestore(),d.getCreateTime(),d.getCanSell(),d.getStatus()};
		return driverDao.save(sql,parmas,d);
		
	}
	
	public void update(Driver d) {
		String sql = "update driver set name=?,mobile=?,credit=?,non_restore=?,create_time=?,can_sell=?,status=? where id=?";
		Object[] params = {d.getName(),d.getMobile(),d.getCredit(),d.getNonRestore(),d.getCreateTime(),d.getCanSell(),d.getStatus(),d.getId()};
		driverDao.update(sql, params, d);
	}
	
	public Driver findBycarId(String carId){
		String sql = "select * from driver where status=1 and car_id=? ";
		Object[] params = {carId};
		
		return driverDao.findBycarId(sql,params);
		
	}

	public void updateStatus(long id, int status) {
		String sql = "update driver set status=? where id=?";
		Object[] parmas ={status,id};
		driverDao.updateStatus(sql,parmas);
	}
	
	public int addNonRestore(int id, int nonRestore) {
		return driverDao.addNonRestore(id, nonRestore);
	}
	
	public Driver findById(int id) {
		return driverDao.findById(id);
	}
	
	public Driver findByMobile(String mobile) {
		return driverDao.findByMobile(mobile);
	}
	
	public int updatePwd(int id, String pwd) {
		return driverDao.updatePwd(id, pwd);
	}

	public List selectCanDispatch(int money) {
		return driverDao.selectCanDispatch(money);
	}

	public boolean updateNonRestore(String carId, int money) {
		return driverDao.updateNonRestore(carId, money);
	}
}
