package tw.com.twgame.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import tw.com.twgame.model.Payment;
import tw.com.twgame.model.PaymentForView;


@Repository
public class PaymentDao extends BaseDao {
	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;
	
	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;
	
	@Autowired
	public PaymentDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}
	
	public int findByViewConditionCount(String payOrderId, int payStatus, int restoreStatus, String carId, Timestamp startCreateTime, Timestamp endCreateTime) {
		StringBuilder condition = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		
		addPayOrderId(condition, params, payOrderId);
		addPayStatus(condition, params, payStatus);
		addRestoreStatus(condition, params, restoreStatus);
		addCarId(condition, params, carId);
		addMinCreateTime(condition, params, startCreateTime);
		addMaxCreateTime(condition, params, endCreateTime);
		
		String sql = " SELECT COUNT(*) FROM `payment` WHERE 1=1 " + condition.toString();
		return queryForInt(dsMnMainQry, sql, params.toArray());
	}
	
	public List<PaymentForView> findByViewCondition(String payOrderId, int payStatus, int restoreStatus, String carId, Timestamp startCreateTime, Timestamp endCreateTime, int pageNo, int pageSize) {
		StringBuilder condition = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		
		addPayOrderId(condition, params, payOrderId);
		addPayStatus(condition, params, payStatus);
		addRestoreStatus(condition, params, restoreStatus);
		addCarId(condition, params, carId);
		addMinCreateTime(condition, params, startCreateTime);
		addMaxCreateTime(condition, params, endCreateTime);
		
		String sql = "SELECT payment1.`id` `id`, money, currency, user_address, car_id, pay_from, payment1.create_time create_time, restore_status, payment1.`name` `name`, user_mobile, remark, game.`name` game_name, payment1.uid uid, account_status, pay_status, order_id, cancel_time, pay_order_id, driver_id, is_first, pay_time, member.`status` member_status FROM ( "
				+ " (SELECT * FROM `payment` WHERE 1=1 "
				+ condition.toString()
				+ " ) payment1 "
				+ " LEFT JOIN game ON payment1.game_code = game.game_code "
				+ " LEFT JOIN member ON payment1.uid = member.uid) "
				+ " ORDER BY payment1.id DESC LIMIT ?, ?";
		
		params.add((pageNo-1)*pageSize);
		params.add(pageSize);
		return queryForList(dsMnMainQry, sql, params.toArray(), PaymentForView.class);
	}

	private void addMaxCreateTime(StringBuilder condition, ArrayList<Object> params, Timestamp endCreateTime) {
		if(endCreateTime == null) {
			return;
		} else {
			condition.append(" AND create_time >= ? ");
			params.add(endCreateTime);
		}
	}

	private void addMinCreateTime(StringBuilder condition, ArrayList<Object> params, Timestamp startCreateTime) {
		if(startCreateTime == null) {
			return;
		} else {
			condition.append(" AND create_time <= ? ");
			params.add(startCreateTime);
		}
	}

	private void addCarId(StringBuilder condition, ArrayList<Object> params, String carId) {
		if(carId == null || carId.length() == 0) {
			return;
		} else {
			condition.append(" AND car_id= ? ");
			params.add(carId);
		}
	}

	private void addRestoreStatus(StringBuilder condition, ArrayList<Object> params, int restoreStatus) {
		if(restoreStatus == -1) {
			return;
		} else {
			condition.append(" AND restore_status= ? ");
			params.add(restoreStatus);
		}
	}

	private void addPayStatus(StringBuilder condition, ArrayList<Object> params, int payStatus) {
		if(payStatus == -1) {
			return;
		} else {
			condition.append(" AND pay_status = ? ");
			params.add(payStatus);
		}
	}

	private void addPayOrderId(StringBuilder condition, ArrayList<Object> params, String payOrderId) {
		if(payOrderId == null || payOrderId.length() == 0) {
			return;
		} else {
			condition.append(" AND pay_order_id = ? ");
			params.add(payOrderId);
		}
	}

	public PaymentForView findOneById(long id) {
//		StringBuilder condition = new StringBuilder();
//		ArrayList<Object> params = new ArrayList<Object>();
		
		String sql = "SELECT payment1.`id` `id`, money, currency, user_address, car_id, pay_from, payment1.create_time create_time, restore_status, payment1.`name` `name`, user_mobile, remark, game.`name` game_name, payment1.uid uid, account_status, pay_status, order_id, cancel_time, pay_order_id, driver_id, is_first, pay_time, member.`status` member_status FROM ( "
				+ " (SELECT * FROM `payment` WHERE payment.id = ? "
				+ " ) payment1 "
				+ " LEFT JOIN game ON payment1.game_code = game.game_code "
				+ " LEFT JOIN member ON payment1.uid = member.uid) ";
		return (PaymentForView) queryForList(dsMnMainQry, sql, new Object[]{id}, PaymentForView.class).get(0);
	}

	public int updatePayStatus(long id, int payStatus) {
		String sql = "UPDATE payment SET pay_status=? WHERE id=?";
		return updateForObject(dsMnMainUpd, sql, new Object[] {payStatus, id});
	}
	
	public int updatePayStatus(long id, int nowPayStatus, int newPayStatus) {
		String sql = "UPDATE payment SET pay_status=? WHERE id=? AND pay_status=?";
		return updateForObject(dsMnMainUpd, sql, new Object[] {newPayStatus, id, nowPayStatus});
	}

	public void updateRemarkById(long id, String remark) {
		String sql = "UPDATE payment SET remark=? WHERE id=?";
		updateForObject(dsMnMainUpd, sql, new Object[] {remark, id});
	}

	public Payment add(final Payment pay) {
		final String sql = 
				"INSERT INTO payment("
				+"order_id,pay_from,pay_order_id,uid,game_code,money,currency,"
				+ "name,user_mobile,user_address,create_time,is_first) values "+
				"(?,?,?,?,?,?,?,?,?,?,?,?)";
		
		return (Payment) addForObject(dsMnMainUpd, sql, pay, new Object[] { 
				pay.getOrderId(), pay.getPayFrom(), pay.getPayOrderId(), pay.getUid(), 
				pay.getGameCode(), pay.getMoney(), pay.getCurrency(), pay.getName(), pay.getUserMobile(),
				pay.getUserAddress(), pay.getCreateTime(), pay.getIsFirst() });
	}
	
	public Payment addPaid(final Payment pay, String channel) {
		final String sql = 
				"INSERT INTO payment("
				+"order_id,pay_from,pay_order_id,uid,game_code,money,currency,"
				+ "name,user_mobile,user_address,create_time,is_first,pay_status,account_status,channel) values "+
				"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		return (Payment) addForObject(dsMnMainUpd, sql, pay, new Object[] { 
				pay.getOrderId(), pay.getPayFrom(), pay.getPayOrderId(), pay.getUid(), 
				pay.getGameCode(), pay.getMoney(), pay.getCurrency(), pay.getName(), pay.getUserMobile(),
				pay.getUserAddress(), pay.getCreateTime(), pay.getIsFirst(), pay.getPayStatus(), 
				pay.getAccountStatus(), channel });
	}
	
	public int countPay(String uid) {
		final String sql = "select count(*) from payment where pay_status>1 and uid=?";
		return this.queryForInt(dsMnMainQry, sql, new Object[] {uid});
	}
	
	public void updateRestore(String[] payList, Date date, int rstatus) {
		StringBuilder sb = new StringBuilder();
		sb.append("update payment set restore_time=?, restore_status=? where order_id in (");
		
		for(String payId : payList) {
			sb.append("'").append(payId).append("',");
		}
		
		sb.deleteCharAt(sb.length()-1);
		sb.append(")");
		super.updateForObject(dsMnMainUpd, sb.toString(), new Object[]{date, rstatus});
	}
	
	public void updateRestore(String[] payList, int ptatus, int rstatus) {
		StringBuilder sb = new StringBuilder();
		sb.append("update payment set pay_status=?, restore_status=? where order_id in (");
		
		for(String payId : payList) {
			sb.append("'").append(payId).append("',");
		}
		
		sb.deleteCharAt(sb.length()-1);
		sb.append(")");
		super.updateForObject(dsMnMainUpd, sb.toString(), new Object[]{ptatus, rstatus});
	}

	public boolean updateCarIdDerverID(Long id, String carId, Long derverId) {
		String sql = "UPDATE payment SET car_id=?, driver_id=? WHERE id=?";
		return updateForObject(dsMnMainUpd, sql, new Object[] {carId, derverId, id}) > 0;
	}

	public Payment findByPayFromPayOrderId(String payFrom, String orderId) {
		return (Payment) queryForObject(dsMnMainQry, "SELECT * FROM payment WHERE pay_from=? AND pay_order_id=?", new Object[]{payFrom, orderId}, Payment.class);
	}

	public PaymentForView findByOrderId(String orderId) {
		String sql = "SELECT payment1.`id` `id`, money, currency, user_address, car_id, payment1.pay_from, payment1.create_time create_time, restore_status, payment1.`name` `name`, user_mobile, remark, game.`name` game_name, payment1.uid uid, account_status, pay_status, order_id, cancel_time, pay_order_id, driver_id, is_first, pay_time, IFNULL(member.`status`,0) member_status, is_test FROM ( "
				+ " (SELECT * FROM `payment` WHERE payment.order_id = ? "
				+ " ) payment1 "
				+ " LEFT JOIN game ON payment1.game_code = game.game_code "
				+ " LEFT JOIN member ON (payment1.uid = member.uid AND payment1.pay_from = member.pay_from)) ";
		Object[] args = new Object[]{orderId};
		return (PaymentForView) queryForObject(dsMnMainQry, sql, args, PaymentForView.class);
	}
}
