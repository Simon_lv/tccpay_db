package tw.com.twgame.dao;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import tw.com.twgame.model.Member;


@Repository
public class MemberDao extends BaseDao{
	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;
	
	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;
	
	@Autowired
	public MemberDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}
	
	public int authenticate(String uid) {
		String sql = "UPDATE member SET `status`=1 WHERE uid=?";
		return updateForObject(dsMnMainUpd, sql, new Object[] {uid});
	}
	
	public Member findByUid(final String uid) {
		final String sql = "select * from member where uid=?";
		return (Member) queryForObject(dsMnMainQry, sql, new Object[] {uid} , Member.class);
	}
	
//	public Member add(final Member member) {
//		final String sql = 
//				"INSERT INTO member("
//				+"uid,name,create_time) values "+
//				"(?,?,NOW())";
//		
//		return (Member) addForObject(dsMnMainUpd, sql, member, new Object[] { 
//				member.getUid(), member.getName() });
//	}

	public Member add(final Member member) {
		final String sql = 
				"INSERT INTO member("
				+"uid,name,create_time, pay_from, mobile, address, last_pay_time ) values "+
				"(?,?,NOW(), ?, ?, ?, ?)";
		
		return (Member) addForObject(dsMnMainUpd, sql, member, new Object[] { 
				member.getUid(), member.getName(), member.getPayFrom(), member.getMobile(), member.getAddress(),  member.getLastPayTime()});
	}

	public Member findByUidAndPayFrom(String uid, String payFrom) {
		final String sql = "select * from member where uid=? AND pay_from=?";
		return (Member) queryForObject(dsMnMainQry, sql, new Object[] {uid, payFrom} , Member.class);
	}

	public int update(Member member) {
		final String sql = "UPDATE `member` SET `name`=?, `mobile`=?, `address`=?, `last_pay_time`=? WHERE `uid`=? AND `pay_from`=?";
		return updateForObject(dsMnMainUpd, sql, new Object[]{
				member.getName(), member.getMobile(), member.getAddress(), member.getLastPayTime(), member.getUid(), member.getPayFrom()});
	}

	public Member findByNameMobilePayFrom(String name, String mobile, String payFrom) {
		final String sql = "select * from member where `name`=? AND mobile=? AND pay_from=?";
		return (Member) queryForObject(dsMnMainQry, sql, new Object[] {name, mobile, payFrom} , Member.class);
	}

	public int updateUid(String uid, String name, String mobile, String payFrom) {
		final String sql = "UPDATE `member` SET `uid`=? WHERE `name`=? AND `mobile`=? AND pay_from = ?";
		return updateForObject(dsMnMainUpd, sql, new Object[]{uid, name, mobile, payFrom});
	}

}
