package tw.com.twgame.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import tw.com.twgame.model.Customer;

@Repository
public class CustomerDao extends BaseDao {
	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;
	
	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;
	
	@Autowired
	public CustomerDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}
	

	public Customer findByPayFrom(String payFrom) {
		final String sql = "select * from customer where pay_from=?";
		return (Customer) queryForObject(dsMnMainQry, sql, new Object[] {payFrom} , Customer.class);
	}


	public List queryAllPayFrom() {
		final String sql = "select pay_from from customer ORDER BY pay_from";
		return queryForList(dsMnMainQry, sql, new Object[] {} , String.class);
	}

}
