package tw.com.twgame.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

public class FullOrderRowMapper implements RowMapper<Map<String, String>> {

	@Override
	public Map<String, String> mapRow(ResultSet rs, int num) throws SQLException {
		Map<String, String> result = new HashMap<String, String>();
		//orderid productName receiveName receiveMobile itemName price amount money orderCreateTime payTime orderStatus memberStatus
		result.put("orderid", rs.getString("orderid"));
		result.put("productName", rs.getString("productName"));
		result.put("receiveName", rs.getString("receiveName"));
		result.put("receiveMobile", rs.getString("receiveMobile"));
		result.put("receiveAddress", rs.getString("receiveAddress"));
		result.put("itemName", rs.getString("itemName"));
		result.put("price", rs.getString("price"));
		result.put("amount", rs.getString("amount"));
		result.put("money", rs.getString("money"));
		result.put("orderCreateTime", rs.getString("orderCreateTime"));
		result.put("payTime", rs.getString("payTime"));
		result.put("orderStatus", rs.getString("orderStatus"));
		result.put("memberStatus", rs.getString("memberStatus"));
		result.put("memberName", rs.getString("memberName"));
		result.put("memberMobile", rs.getString("memberMobile"));
		result.put("memberEmail", rs.getString("memberEmail"));
		result.put("memo", rs.getString("memo"));
		return result;
	}

}
