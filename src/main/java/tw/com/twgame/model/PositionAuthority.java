package tw.com.twgame.model;


public class PositionAuthority extends BaseEntity{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8939726078717500955L;
	private int positionId;
	private int authorityId;
	private int status;
	
	public int getPositionId() {
		return positionId;
	}
	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}
	public int getAuthorityId() {
		return authorityId;
	}
	public void setAuthorityId(int authorityId) {
		this.authorityId = authorityId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

}
